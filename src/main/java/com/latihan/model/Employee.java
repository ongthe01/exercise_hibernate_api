package com.latihan.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name="employee")
@DynamicInsert

public class Employee implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)// Primary Key
	private Long id;
	
	@Column(name="employeename", columnDefinition = "VARCHAR(255)") // name not null
	@NotNull
	private String name;
	
	@Column(name="address", columnDefinition = "VARCHAR(255)") // address
	private String address;
	
	@Column(name="phone", columnDefinition = "VARCHAR(14)") //phone
	private String phone;
	
	@Column(name="emergencycontact", columnDefinition = "VARCHAR(155)", nullable = true) // nullable
	private String emergencyContact; 
	
	@Column(name="salary") //salary
	private int salary;
	
	@Column(name="startat") //start at
	private Date startAt;
	
	@Column(name="resignstatus", nullable = true)
	private String resignStatus;
	
	@Column(name="resignat", nullable = true)
	private Date resignAt;
	
	@Column(name="createat", columnDefinition = "TIMESTAMP default NOW()")
	private Date createAt;
	
	@Column(name="createby", columnDefinition = "VARCHAR(255) default 'SYSTEM' ")
	private String createBy;
	
	@Column(name="updateat", columnDefinition = "TIMESTAMP default NOW()")
	private Date updateAt;
	
	@Column(name="updateby", columnDefinition = "VARCHAR(255) default 'SYSTEM' ")
	private String updateBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmergencyContact() {
		return emergencyContact;
	}

	public void setEmergencyContact(String emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public Date getStartAt() {
		return startAt;
	}

	public void setStartAt(Date startAt) {
		this.startAt = startAt;
	}

	public String getResignStatus() {
		return resignStatus;
	}

	public void setResignStatus(String resignStatus) {
		this.resignStatus = resignStatus;
	}

	public Date getResignAt() {
		return resignAt;
	}

	public void setResignAt(Date resignAt) {
		this.resignAt = resignAt;
	}
	
}
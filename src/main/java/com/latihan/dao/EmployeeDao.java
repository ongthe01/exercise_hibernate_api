package com.latihan.dao;

import com.latihan.model.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

public interface EmployeeDao extends JpaRepository<Employee, Long>{
	
}
package com.latihan.controller;

import com.latihan.exception.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.*;

import com.latihan.model.*;
import com.latihan.dao.*;

@RestController
public class EmployeeController {
	@Autowired
	EmployeeDao employeeDao;
	
	@GetMapping("/employee")
	public Page<Employee> getEmployee(Pageable pageable){
		return employeeDao.findAll(pageable);
	}
	
	public String successMessage(String msg) {
		return msg;
	}
	
	//add employee
	@PostMapping("/employee")
	public String addEmployee(@Valid @RequestBody Employee emp) {
		employeeDao.save(emp);
		return successMessage("Successfully added new employees");
	}
	
	//update employee
	@PutMapping("/employee/{employeeId}")
	public Employee updateEmployee(@PathVariable Long employeeId, 
			@Valid @RequestBody Employee empRequest) {
		
		return employeeDao.findById(employeeId)
				.map(emp->{
					emp.setName(empRequest.getName()); //name
					emp.setAddress(empRequest.getAddress()); //address
					emp.setPhone(empRequest.getPhone()); //phone
					emp.setEmergencyContact(empRequest.getEmergencyContact()); //emergency contact
					emp.setSalary(empRequest.getSalary()); //salary
					emp.setResignStatus(empRequest.getResignStatus()); //resign status
					emp.setResignAt(empRequest.getResignAt()); //resign at
					successMessage("Successfully update employee");
					return employeeDao.save(emp);
				}).orElseThrow(()-> new ResourceNotFoundException("Error Employee not found, with id " + employeeId));
				
	}
	
	//delete employee by employee id
	@DeleteMapping("/employee/{employeeId}")
	public ResponseEntity<?> deleteEmployee(@PathVariable Long employeeId){
		return employeeDao.findById(employeeId)
				.map(emp -> {
					successMessage("Successfully delete employee");
					employeeDao.delete(emp);
					return ResponseEntity.ok().build();
				}).orElseThrow(()-> new ResourceNotFoundException("Error Employee not found, with id " + employeeId));
	}
	
}